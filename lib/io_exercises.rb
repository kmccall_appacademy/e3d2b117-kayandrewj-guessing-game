# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.

# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  target = 1 + rand(100)
  tries = 0
  guess = 0
  while guess != target
    puts "Guess a number"
    guess = gets.chomp.to_i
    puts "Your guess is #{guess}"
    puts "Your guess is too high" if guess > target
    puts "Your guess is too low" if guess < target
    tries += 1
  end
  puts "You guessed the number #{target}"
  puts "It took #{tries} tries"

end

def file_shuffler
  puts "Which file would you like to shuffle?"
  selected_file = gets
  line_array = File.readlines(selected_file)
  line_array..shuffle.join("")
end
